$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/sinnerFeature.feature");
formatter.feature({
  "name": "Verify the sinnerSchrader functionality",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.scenario({
  "name": "Evaluate the broken images out of all images and assert the result.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "enter the image URL on the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "get all the images and assert the broken images",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.java:29"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "quite the browser",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:32"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Perform login with Basic Auth",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "enter the login URL on the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:35"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter the userName and password",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.java:38"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "assert the login success message",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:41"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Move slider to max and min and assert the value",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "enter the slider URL on the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:44"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "move the slider to max and assert the value",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:47"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "move the slider to min and assert the value",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.java:51"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Hover on the pictures and assert the details like User",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BasicFeatures"
    }
  ]
});
formatter.step({
  "name": "enter the hover URL on the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:55"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "mouse hover on the images and get the details of the images",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:58"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "close the browser",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:61"
});
formatter.result({
  "status": "passed"
});
});