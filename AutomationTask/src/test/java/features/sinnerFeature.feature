@BasicFeatures
Feature: Verify the sinnerSchrader functionality
  Scenario: Evaluate the broken images out of all images and assert the result.
  Given enter the image URL on the browser
  When  get all the images and assert the broken images
  Then quite the browser

  Scenario: Perform login with Basic Auth
  Given enter the login URL on the browser
  When enter the userName and password
  Then assert the login success message

  Scenario: Move slider to max and min and assert the value
  Given enter the slider URL on the browser
  Then move the slider to max and assert the value
  And  move the slider to min and assert the value

  Scenario: Hover on the pictures and assert the details like User
  Given enter the hover URL on the browser
  Then mouse hover on the images and get the details of the images
  Then close the browser
