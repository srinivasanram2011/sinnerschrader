import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        plugin = {"pretty","html:target/cucumber_1","timeline:target/timelineReport"},
        features = "src/test/java/features",
        tags="@BasicFeatures",
        glue = {"step_definitions"},
        junit = {"--step-notifications"})


public class Runners {
}
