package pageObjectModel;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class CommonFunctions extends BaseTest {
    @FindBy(xpath = "//div[@class=\"example\"]//img")
    static List<WebElement> collectImages;
    @FindBy(xpath = "//div[@class=\"example\"]//p")
    static WebElement getBasicAuthMessage;
    @FindBy(xpath = "//*[@class=\"sliderContainer\"]//input")
    static WebElement sliderIcon;
    @FindBy(xpath = "//span[@id=\"range\"]")
    static WebElement getSliderValue;
    @FindBy(xpath = "//*[@class=\"figure\"]")
    static List<WebElement> getAllFigures;
    @FindBy(xpath = "//*[@class=\"figcaption\"]//h5")
    static List<WebElement> getUserDetails;

    public void initialRequest(String browser) {

        switch (browser) {
            case "CHROME":
                WebDriverManager.getInstance(CHROME).setup();
                DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
                acceptSSlCertificate.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                driver = new ChromeDriver(acceptSSlCertificate);
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                break;
            case "FIREFOX":
                WebDriverManager.firefoxdriver().setup();
                DesiredCapabilities acceptSSlCertificates = DesiredCapabilities.firefox();
                acceptSSlCertificates.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                break;

        }
        PageFactory.initElements(driver, CommonFunctions.class);

    }

    public void launchApplication(String url, String browser) {
        initialRequest(browser);
        driver.get(url);

    }

    public void verifyImages() throws IOException {
        for (int i = 0; i <= collectImages.size()-1 ; i++) {
            String url = collectImages.get(i).getAttribute("src");
            System.out.println();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            if (String.valueOf(con.getResponseCode()).equals("200")) {
                Assert.assertEquals(con.getResponseCode(), 200);

            } else
                Assert.assertEquals(con.getResponseCode(), 404);

        }
    }

    public void quiteBrowser() {
        driver.quit();
    }

    public void loginFns(String userName, String password, String browser) {
        String baseURL = "https://" + userName + ":" + password + "@the-internet.herokuapp.com/basic_auth";
        launchApplication(baseURL, browser);


    }

    public void verifyBasicAuthSuccessMessage() {
        System.out.println("fdshffgsfydf" + getBasicAuthMessage.getText());
        Assert.assertEquals("Congratulations! You must have the proper credentials.", getBasicAuthMessage.getText());

    }

    public void moveSliderRight() {
        Actions action = new Actions(driver);
        action.clickAndHold(sliderIcon);
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
            action.sendKeys(Keys.ARROW_RIGHT).build().perform();
        }
        Assert.assertEquals("5", getSliderValue.getText());
    }

    public void moveSliderLeft() {
        Actions action = new Actions(driver);
        action.clickAndHold(sliderIcon);
        for (int i = 0; i < 5; i++) {
            action.sendKeys(Keys.ARROW_LEFT).build().perform();
        }
        Assert.assertEquals("0", getSliderValue.getText());
    }

    public void mouseHoverAndGetDetails() {
        for (int i = 0; i <= getAllFigures.size()-1; i++) {
            Actions action = new Actions(driver);
            action.moveToElement(getAllFigures.get(i)).build().perform();
            String userName = getUserDetails.get(i).getText();
            Assert.assertEquals(userName, "name: user" + (i+1));
        }
    }

}










